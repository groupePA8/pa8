# PA8

Projet d'innovation EFREI 2018-2019

Expression générale du service

Dans le cadre d’un projet scolaire en équipe, nous avons imaginé une plateforme web (voir application mobile dans un développement ultérieur). Celle-ci propose au banque un service de tri des demandeurs de prêt étudiant. 
Notre service proposerait, dans un premier temps, la création et simulation d’un dossier de prêt bancaire pour les étudiants. Ensuite, dans un second temps, classerait pour les banques, les candidats en fonction de leurs dossiers et des critères d’obtention de prêt. 
Notre but principal est de proposer un pré-traitement des dossiers afin d’optimiser l’efficacité des agences bancaires. Et, de rendre beaucoup plus accessible et interactif les démarches administratives au près des demandeurs de prêt. 
